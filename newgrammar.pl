%%
%%  Prolog grammar for MI practical.
%% runs in sictstus v4 on DICE, and swipl 7.4.
%% See end of miNew.pl for documentation on how to use this
%% to check and generate melodies according to the grammar.
%% This file should be in same directory as readfile.pl and miNew.pl
%%
%% Start prolog and consult this file:
%%
%% ?- [newgrammar].
%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%     ORIGINAL GRAMMAR: ACCEPTED INPUTS     %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Obtained by calling 'make_new_with_header('jig.abc','new.abc').', as this command
% implicitly calls 'random_tune', which follows the specified grammar.

% Input 1
% d1b1f1A3|a1f1e1a1a1e1|A1B1f1f3|f1f1a1a1b1d1|a2A1d3|B1e1B1d1e1B1|d2f1A3|f2a1A3|

% Input 2
% a2B1f3|B1e1b1g1g1g1|B1e1d1g1e1d1|f2B1f3|a2B1d3|e1f1a1e1f1A1|f1B1a1a2a1|f1e1A1a2d1|

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%   ORIGINAL GRAMMAR: INPUTS NOT ACCEPTED   %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Tested by calling the 'get_notes' and 'tune' command, as specified in miNew.pl.

% Failed Input 1
% a2g1f3|B1e1b1g1g1g1|B1e1d1g1e1d1|f2B1f3|a2B1d3|e1f1a1e1f1A1|f1B1a1a2a1|f1e1A1a2d1|

% This input is largely identical to the accepted Input 2, but the second note
% of the first bar (originally a B) is changed to a g.
% The parsing fails when attempting to pick a suitable tonic variant for the first
% bar (which is always defined to be a tonic). All of the specified variants allow
% for the second note to be only ton() or by_ton(), and neither of these allow
% the use of g.

% Failed Input 2
% a2B1f1f1f1|B1e1b1g1g1g1|B1e1d1g1e1d1|f2B1f3|a2B1d3|e1f1a1e1f1A1|f1B1a1a2a1|f1e1A1a2d1|

% This input is again similar to the aforementioned Input 2, but the third note
% of the first bar (f3) is split into three identical notes (f1f1f1).
% The parsing again fails due to no suitable tonic variants, though instead of 
% an invalid note, the issue here is due to an invalid rhythmical variation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%             MODIFIED GRAMMAR              %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The following modifications were made:
% 1) The sixth harmony was added, following the subd/dom rhythmical rules.
% 2) The range of notes was moved lower, spanning from E to f instead of A to b.
% 3) Three additional rhythmical variations for tonic were added.

%% special treatment for sum_int/3

:- multifile sum_int/3.
:- dynamic sum_int/3.
:- consult( [miNew, readfile] ).

% make various grammar categories dynamic;
% need to make similar declarations for any new grammar symbols
% that are introduced.

:- dynamic tune/2, line/2, bar1/2, bar/2, bar4/2, tonic/2, dominant/2.
:- dynamic tonicA/2,tonicB/2,tonicC/2,tonicD/2.
:- dynamic subdominant/2, ton/3, by_ton/3, dom/3, by_dom/3, subd/3, by_subd/3.
:- dynamic tonicA/1, tonicB/2, tonicC/2, tonicD/2, tonicE/2, tonicF/2, tonicG/2.
:- dynamic sixth/2, six/3, by_six/3.

% grammar allowing rhythmic variation

tune --> line, line.
line --> bar1, bar, bar, bar4.

bar1 --> tonic.
bar  --> tonic.
bar  --> subdominant.
bar  --> dominant.
bar  --> sixth.
bar4 --> tonic.

tonic --> tonicA.
tonic --> tonicB.
tonic --> tonicC.
tonic --> tonicD.
tonic --> tonicE.
tonic --> tonicF.
tonic --> tonicG.

% parametrised by duration;
% unary notation allows randomisation of output to work:
% " s(1) " for 2, "s(s(1))" for 3, etc (up to 6).
%
%  Use constraints on durations using special sum_int/2 predicate as shown
%  in curly braces " {..} ". 

tonicA --> ton(1),by_ton(1),ton(1),ton(1),by_ton(1),ton(1),[bl].
tonicB --> ton(1),by_ton(1),ton(1), ton(A),ton(B), [bl],
	   { sum_int(A, B, s(s(1))) }.
tonicC --> ton(s(1)),by_ton(1),ton(s(s(1))),[bl].
tonicD --> ton(1),by_ton(1),ton(1),ton(s(s(1))),[bl].
tonicE --> ton(s(1)),by_ton(1),ton(1),by_ton(1),ton(1),[bl].
tonicF --> ton(s(1)),by_ton(1),ton(s(1)),by_ton(1),[bl].
tonicG --> ton(s(s(s(s(s(1)))))),[bl].
dominant --> dom(1),by_dom(1),dom(1),dom(1),by_dom(1),dom(1),[bl].
subdominant --> subd(1),by_subd(1),subd(1),subd(1),by_subd(1),subd(1),[bl].
sixth --> six(1),by_six(1),six(1),six(1),by_six(1),six(1),[bl].

ton(X) --> [d(X)].
ton(X) --> [f(X)].
ton(X) --> ['A'(X)].
ton(X) --> ['F'(X)].
by_ton(X) --> ['B'(X)].
by_ton(X) --> [e(X)].
by_ton(X) --> ['E'(X)].
by_ton(X) --> ton(X).

dom(X) --> [c(X)].
dom(X) --> [e(X)].
dom(X) --> ['A'(X)].
dom(X) --> ['E'(X)].
by_dom(X) --> [f(X)].
by_dom(X) --> ['F'(X)].
by_dom(X) --> dom(X).

subd(X) --> [d(X)].
subd(X) --> ['B'(X)].
subd(X) --> ['G'(X)].
by_subd(X) --> [e(X)].
by_subd(X) --> ['E'(X)].
by_subd(X) --> subd(X).

six(X) --> [d(X)].
six(X) --> [f(X)].
six(X) --> ['B'(X)].
six(X) --> ['F'(X)].
by_six(X) --> ['G'(X)].
by_six(X) --> six(X).

%% end of grammar

% Newly accepted Input 1
% f1e1d1d2d1|f1F1A1d3|d1E1A1A1d1f1|f2A1d1e1A1|f1F1F1A1d2|G1E1d1d1E1d1|f1f1d1F1f1f1|F2e1f3|
% This input cannot be accepted by the original grammar due to lower notes and
% a new rhythmical variation in bar 4.

% Newly accepted Input 2
% d2f1f3|F1e1f1d3|d1G1d1B1e1d1|F2E1d1e1A1|F1E1F1A3|d1B1F1A1f2|B1G1B1F1G1F1|d2d1f1e1d1|
% This input cannot be accepted by the original grammar due to lower notes, new
% rhythmical variations in bars 4 and 8, and a new harmonic in bar 7.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%                DISCUSSION                 %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The reasoning for the undertaken modifications is as follows:
% 1) A new harmonic allows for additional variation for non-tonic bars.
% 2) The range was changed due to the vast majority of identified D major 6/8 jigs
%    commonly using these lower notes. The range was not extended in order to
%    prevent jumps over very large intervals.
% 3) The new rhythmical variations are taken from the following jig example:
% http://abcnotation.com/tunePage?a=trillian.mit.edu/~jc/music/abc/Contra/KC/jig/AndyDeJarlisJ_D/0000
%    This increases the degree of similarity between the generated music and
%    existing jig variants.

% Overall, the added changes do make the resulting piece more like the actual
% examples, compared to the pieces generated from the original grammar. This is
% primarily due to the wider variety of rhythmical structures characteristic of
% a jig piece. In contrast, the lowered range merely shifts the tone of the piece
% without affecting its style. Additionally, the new harmonic changes the sound
% of only individual bars, which may be easily turned into one of the other two
% harmonics with only several note changes.
%
% Nonetheless, the generated pieces can still be improved to further match the
% style of existing jigs. Compared to the above example (among others), the generated
% jigs have a faster tempo, larger jumps between adjacent notes, and no special
% handling of the last bar of the piece, which generally ends with a 1/4, 3/8 or
% 3/4 note. The tempo can be easily changed in the abc header, but the other two
% issues require further modifications of the grammar:
%
% - Reducing the jump interval may be solved by splitting the ton()/by_ton() and
%   similar structures into higher and lower pitch versions, and subsequently doubling
%   the count of tonicA and similar structures, where each is limited to only either
%   the lower or higher pitch notes.
%
% - Handling the last bar would require splitting the line() structure into line1
%   and line2, where line2 would have its last bar limited to only the tonic
%   variants with a fitting rhythmical structure.
%
% It is expected that the above changes would make the generated pieces match actual
% jigs even further, though it is also possible to include further modifications
% of greater complexity, such as structural/note repetition for bar groups/lines.
% This would allow for patterns like in the aforementioned example jig, where
% bars 1-2 and 5-6 are nearly identical save for their last notes.
