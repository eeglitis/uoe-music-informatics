# Music Informatics - 5th Year Informatics Coursework #

This coursework had two primary objectives, both related to automated music generation.

* Objective 1: given an existing Prolog-based grammar for generating jigs, make several different modifications to it, and explain the reasoning for these changes.
* Objective 2: write a short review for a provided article on probabilistic grammars, and explain the strengths and weaknesses of this approach.